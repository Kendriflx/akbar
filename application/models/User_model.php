<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public $id;
	public $username;
	public $password;

	public function get_last_ten_users()
	{
			$query = $this->db->get('users', 10);
			return $query->result();
	}

	public function insert_entry()
	{
			$this->id    = $_POST['id']; // please read the below note
			$this->username  = $_POST['username'];
			$this->password     = md5($_POST['password']);

			$this->db->insert('users', $this);
	}

	public function update_entry()
	{
			$this->title    = $_POST['title'];
			$this->content  = $_POST['content'];
			$this->date     = time();

			$this->db->update('users', $this, array('id' => $_POST['id']));
	}

	public function delete_entry()
	{
		$this->db->delete('users', array('id' => $id));
	}

}

